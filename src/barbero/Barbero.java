package barbero;

/**
 * Clase de tipo [Barbero].
 * Esta clase interactúa con todos los clientes que llegan.
 * <p>
 * Esta clase extiende de {@link Thread} para que no bloquee el proceso de otros objetos
 */
public class Barbero extends Thread {

	/**
	 * Total de tiempo que se puede tardar entre cliente
	 */
	private static final float TIEMPO_MAXIMO_ENTRE_CLIENTE = 20f;

	/**
	 * Total de tiempo que espera hasta dormirse
	 */
	private static final float TIEMPO_ESPERA = 5f;

	/**
	 * Tiempo para realizar cálculos de espera
	 */
	private long ultimoTiempo = System.currentTimeMillis();

	/**
	 * Tiempo que toma entre cada acción
	 */
	private float tiempoEntreAccion = 0f;

	/**
	 * Barbería en la que trabaja
	 */
	private final Barberia barberia;

	/**
	 * Propiedad que proporciona el estado actual del barbero
	 */
	private volatile Estado estado = Estado.DORMIDO;

	/**
	 * Cliente que actualmente está activo
	 */
	private volatile Cliente clienteActual;

	/**
	 * En java no existe un objeto Mutex como tal, pero se puede
	 * realizar este mismo comportamiento con un objeto vació y con la palabra reservada
	 * {@code synchronized} para sincronizar información entre varios hilos
	 */
	private final Object mutex = new Object();

	/**
	 * Constructor por defecto
	 *
	 * @param local La barbería en la que trabaja
	 */
	public Barbero(Barberia local) {
		barberia = local;
	}

	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */

	/**
	 * Ejecución del hilo del barbero
	 */
	@SuppressWarnings({"InfiniteLoopStatement", "BusyWait"})
	@Override
	public void run() {
		// Realizamos un bucle infinito
		while (true) {
			// Si el barbero esta dormido entonces no realiza ninguna acción
			if (estado == Estado.DORMIDO) continue;

			// Calculamos la diferencia de tiempo desde la última ejecución hasta el momento actual,
			// esto lo dividimos entre 1000 debido a que la diferencia es en milisegundos y lo queremos en segundos.
			long tiempoActual = System.currentTimeMillis();
			long transcurrido = tiempoActual - ultimoTiempo;
			float tiempoSegundos = transcurrido / 1000f;

			// Cada vez que se calcula el tiempo se incrementa [tiempoEntreAccion] que es el encargado de verificar
			// cuanto tiempo lleva el barbero esperando sin hacer nada
			ultimoTiempo = tiempoActual;
			tiempoEntreAccion += tiempoSegundos;

			// Creamos un bucle infinito para manejar las acciones del barbero
			switch (estado) {
				// Verificamos cada uno de los estados del barbero
				case LIBRE:
					// Sincronizamos esta parte para no tener problemas de acceso
					// es por eso que utilizamos el objeto mutex para realizar la sincronización
					synchronized (mutex) {
						// Verificamos si el siguiente cliente existe
						Cliente siguiente = barberia.siguienteCliente();
						// Si el cliente no existe y además el tiempo es mayor al que el barbero necesita
						// para dormirse, entonces el barbero se duerme
						if (siguiente == null && tiempoEntreAccion > TIEMPO_ESPERA) {
							// Cambiamos el estado (importante)
							estado = Estado.DORMIDO;
							// Reiniciamos el tiempo
							tiempoEntreAccion = 0;
							System.out.println("El barbero se canso de esperar y se durmió.");
							continue;
						}
						// Si el cliente existe, eso quiere decir que es el siguiente por atender
						if (siguiente != null) {
							// Asignamos al cliente (importante)
							clienteActual = siguiente;
							// Cambiamos el estado (importante)
							estado = Estado.OCUPADO;
						}
					}
					break;
				case OCUPADO:
					// Cuando el barbero está ocupado, necesitamos dormir el hilo actual.
					// Esto para simular que el barbero está trabajando con el cliente y dicho tiempo
					// se calcula de manera pseudo-aleatoria entre un rango de tiempo especificado en [TIEMPO_MAXIMO_ENTRE_CLIENTE]
					long hiloTiempoDormido = (long) Math.floor(Barberia.random.nextFloat() * TIEMPO_MAXIMO_ENTRE_CLIENTE);

					// Tratamos de realizar las acciones y capturar cualquier error
					try {
						// Dormimos el hilo actual
						Thread.sleep(hiloTiempoDormido);
						// Solo sincronizamos este bloque porque es la única acción en el que se utiliza
						// un objeto de otro hilo
						synchronized (mutex) {
							// Le pedimos amablemente al cliente que se vaya
							clienteActual.irse(true);
						}
						// Reiniciamos el tiempo
						tiempoEntreAccion = 0;
						// Eliminamos al cliente del "sillón" para atender
						clienteActual = null;
						// Cambiamos el estado del cliente (importante)
						estado = Estado.LIBRE;
					} catch (Exception ignore) {
					}
			}
		}
	}

	/**
	 * Devuelve el estado actual del barbero
	 *
	 * @return Estado del barbero
	 */
	public Estado getEstado() {
		// Como el estado es accesible desde otros hilos, debe estar sincronizado
		synchronized (mutex) {
			return estado;
		}
	}

	/**
	 * Despierta al barbero.
	 * Se verifica si el barbero está dormido, debido a que posiblemente en alguna ejecución no lo esté
	 * y cambiemos el estado por error, haciendo que el programa deje de funcionar
	 */
	public void despertar() {
		// Verificamos si realmente el barbero está dormido
		if (getEstado() == Estado.DORMIDO) {
			// Cambiamos el estado del barbero
			System.out.println("El cliente despertó al barbero y este empieza a trabajar.");
			estado = Estado.LIBRE;
		}
	}

	/**
	 * Tipo enumerado que identifica el estado que puede tener el barbero
	 */
	public enum Estado {
		DORMIDO,
		OCUPADO,
		LIBRE
	}

}
